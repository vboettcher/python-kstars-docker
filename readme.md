# KStars Python Module Docker Container
This is simply an `ubuntu:devel` container with the kstars python
module installed.

It can be used to build catalogs for kstars at home and in the ci.

## Building
Simply run `docker build .` in the root of the repo and you're golden.

## Docker Hub
The image is built once a week from the latest release branch of
kstars for good measure.

You can access it via `docker pull hiro98/pykstars:latest`.

## Usage
To use the container as an ephemeral development shell just type
`docker run --rm -it hiro98/pykstars:latest` and you're golden.

Head over [here](https://invent.kde.org/vboettcher/kstars-catalogs) to
see another example of usage.
