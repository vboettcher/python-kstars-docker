FROM ubuntu:noble
MAINTAINER Valentin Boettcher hiro@protagon.space

RUN DEBIAN_FRONTEND=noninteractive apt -y update
RUN DEBIAN_FRONTEND=noninteractive apt -y install software-properties-common
RUN DEBIAN_FRONTEND=noninteractive apt-add-repository --yes ppa:mutlaqja/ppa

RUN DEBIAN_FRONTEND=noninteractive apt -y upgrade
RUN DEBIAN_FRONTEND=noninteractive apt -y install python3-full python3-pip kstars git build-essential cmake software-properties-common ninja-build
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install build-essential cmake git libxisf-dev libeigen3-dev libcfitsio-dev zlib1g-dev libindi-dev extra-cmake-modules libkf5plotting-dev libqt5svg5-dev libkf5xmlgui-dev libkf5kio-dev kinit-dev libkf5newstuff-dev libkf5doctools-dev libkf5notifications-dev qtdeclarative5-dev libkf5crash-dev gettext libnova-dev libgsl-dev libraw-dev libkf5notifyconfig-dev wcslib-dev libqt5websockets5-dev xplanet xplanet-images qt5keychain-dev libsecret-1-dev breeze-icon-theme libqt5datavisualization5-dev

# TEMP fix
RUN DEBIAN_FRONTEND=noninteractive apt-get -y remove libstellarsolver2
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install libstellarsolver-dev

# python stuff
RUN DEBIAN_FRONTEND=noninteractive apt -y install python3-pybind11 python3-mypy mypy

# kstars
RUN curl -L https://download.kde.org/stable/kstars/kstars-3.7.0.tar.xz > kstars.tar.xz
RUN tar xvf kstars.tar.xz
RUN bash -c  'mv kstars-* kstars'

WORKDIR kstars
WORKDIR kstars/python/
RUN python3 setup.py install
WORKDIR /

# cleanup
RUN rm -rf kstars
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV PYTHONUNBUFFERED=yes

ENTRYPOINT ["/bin/bash", "-l", "-c"]
